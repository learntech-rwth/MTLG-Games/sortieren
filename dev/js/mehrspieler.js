function mehrspieler(){

	stage.removeAllEventListeners();
	stage.removeAllChildren();
	// 1  1  | 2  4
	// 1  1  | 1  3
	w = stage.canvas.width;
	h = stage.canvas.height;
	var cont;
	switch(anzahlSpieler){

		case 1:
			// single player
			cont = new createjs.Container();
			cont.setBounds(0, 0, w, h);
			waage(cont);
			stage.addChild(cont)
			break;

		case 4:
			cont = new createjs.Container();
			cont.setBounds(0, 0, w/2, h/2);
			cont.setTransform(w, h/2, -0.5, -0.5);
			waage(cont);
			stage.addChild(cont)
		case 3:
			cont = new createjs.Container();
			cont.setBounds(0, 0, w/2, h/2);
			cont.setTransform(w/2, h/2, 0.5, 0.5);
			waage(cont);
			stage.addChild(cont)
		case 2:
			cont = new createjs.Container();
			cont.setBounds(0, 0, w/2, h/2);
			cont.setTransform(w/2, h/2, -0.5, -0.5);
			waage(cont);
			stage.addChild(cont)
		// player 1
			cont = new createjs.Container();
			cont.setBounds(0, 0, w/2, h/2);
			cont.setTransform(0, h/2, 0.5, 0.5);
			waage(cont);
			stage.addChild(cont)
			break;

		default:
			//Fehler, erstmal wieder zur Spielerauswahl
			spielerauswahl();
	}
}
