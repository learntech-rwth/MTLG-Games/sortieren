function einfuehrung() {

	// Hintergrundbild
	var hintergrund = new createjs.Bitmap("img/Hintergrund2K.png");
	hintergrund.scaleX = skB(1);
	hintergrund.scaleY = skH(1);
	stage.addChild(hintergrund);

	// --------------------Erstellen der Buttons--------------------------
	// # Zurueck Button
	var buttonZurueck = new createjs.Shape();
	buttonZurueck.graphics.beginFill("red").drawRoundRect(0,0,skB(buttonBreite),skH(buttonHoehe),skB(20));
	buttonZurueck.shadow = new createjs.Shadow("#000000", 5,5,5);

	var bildZurueck = new createjs.Bitmap("img/pfeillinks.png");
	bildZurueck.scaleX = skB(1);
	bildZurueck.scaleY = skH(1);

	var containerZurueck = new createjs.Container();
	containerZurueck.x = skB(30);
	containerZurueck.y = skH(1080 - buttonHoehe - 30);
	containerZurueck.addChild(buttonZurueck, bildZurueck);
	containerZurueck.addEventListener("click", clickZurueck);
	stage.addChild(containerZurueck);

	// # Loesen/Weiter Button
	var buttonLoesen = new createjs.Shape();
	buttonLoesen.graphics.beginFill("#04B404").drawRoundRect(0,0, skB(buttonBreite), skH(buttonHoehe),skB(20));
	buttonLoesen.shadow = new createjs.Shadow("#000000", 5, 5, 5);

	var bildLoesen = new createjs.Bitmap("img/pfeilrechts.png");
	bildLoesen.scaleX = skB(1);
	bildLoesen.scaleY = skH(1);

	var containerLoesen = new createjs.Container();
	containerLoesen.x = skB(1920-buttonBreite-30);
	containerLoesen.y = skH(1080-buttonHoehe-30);
	containerLoesen.addChild(buttonLoesen, bildLoesen);
	containerLoesen.addEventListener("click", clickLoesen);
	stage.addChild(containerLoesen);

	// # Wischhand (Symbol)
	var hand = new createjs.Bitmap("img/hand.png");
	hand.scaleX = skB(0.8); // hand.breite = 200
	hand.scaleY = skH(0.8); // hand.hoehe = 300
	hand.x = skB(50);
	hand.y = skH(50);
	stage.addChild(hand);

	// --------------- Erstellen der Gewichte und der Waage auf der Stage ----------------------
	// # Gesammelter Haufen von praktischen Variablen
	// Anzahl Gewichte im Einfuehrungsmodus
	var anzahlG = 3;
	// X-Position des ersten und des letzten Gewichts in der Reihe
	var posXminG = skB(300);
	var posXmaxG = skB(1920 - 130) - posXminG;
	// Abstand zwischen den Gewichten
	var deltaposG = (posXmaxG - posXminG)/(anzahlG-1);
	// Y-Position der Gewichtreihe
	var posYG = skH(600);
	// Höhe und Breite der Gewichte
	var hoeheG = skH(220);
	var breiteG = skB(130);
	// Position der Waage (Bild) auf der Stage
	var posXW = skB(710);
	var posYW = skH(30);
	// Hoehe und Breite der Waageboxen auf der Stage
	var breiteW = skB(130);
	var hoeheW = skH(220);
	// Position der Boxen der Waage auf der Stage
	var posXW1 = posXW+skB(43);
	var posYW1 = posYW+skH(168);
	var posXW2 = posXW+skB(330);
	var posYW2 = posYW1;
	// Waagen Ausschlag
	var tilt = skH(23);
	var tiltmin = posYW1 + tilt;
	var tiltmax = posYW1 - tilt;
	var tilt0 = posYW1;
	var tilttime = 800;
	// Anzahl der Gewichte, die in die Waage gelegt worden
	var anzahlW = 0;
	// Position der Elemente
	var position = [0,2,1];

	// --------- Hilfe: was bedeutet der Ausschlag, mit den Bildern links und rechts ----------
	var elefant1 = new createjs.Bitmap("img/elefant.png");
	elefant1.scaleX = skB(1);
	elefant1.scaleY = skH(1);
	elefant1.x = posXW - skB(150); // Position neben der Waage
	elefant1.y = posYW + skH(350);
	elefant1.alpha = 0;
	stage.addChild(elefant1);

	var elefant2 = new createjs.Bitmap("img/elefant.png");
	elefant2.scaleX = skB(1);
	elefant2.scaleY = skH(1);
	elefant2.x = posXW + skB(510); // Position neben der Waage
	elefant2.y = posYW + skH(350);
	elefant2.alpha = 0;
	stage.addChild(elefant2);

	var vogel1 = new createjs.Bitmap("img/vogel.png");
	vogel1.scaleX = skB(1);
	vogel1.scaleY = skH(1);
	vogel1.x = posXW - skB(100); // Position neben der Waage
	vogel1.y = posYW + skH(250);
	vogel1.alpha = 0;
	stage.addChild(vogel1);

	var vogel2 = new createjs.Bitmap("img/vogel.png");
	vogel2.scaleX = skB(1);
	vogel2.scaleY = skH(1);
	vogel2.x = posXW + skB(510); // Position neben der Waage
	vogel2.y = posYW + skH(250);
	vogel2.alpha = 0;
	stage.addChild(vogel2);

	// # Daten fuer die Animation der Waage
	var datenW = {
		images: ["img/animationKomplett.png"],
		frames: {width:500, height:485},
		animations: {
			start: 0,
			links:[0,7, "stand1", 0.2],
			stand1:7,
			linksR:[8,15, "stand2", 0.2],
			stand2:15,
			rechts:[16,23, "stand3", 0.2],
			stand3:23,
			rechtsR:[24,31, "stand4", 0.2],
			stand4:31
		}
	};

	// # Animation der Waage. Faengt im "Ruhezustand" an
	var animationBilder = new createjs.SpriteSheet(datenW);
	var animationW = new createjs.Sprite(animationBilder);
	animationW.x = posXW;
	animationW.y = posYW;
	animationW.scaleX = skB(1);
	animationW.scaleY = skH(1);
	animationW.gotoAndPlay("start");
	stage.addChild(animationW);
	var animationStatus = 0; //Status der Animation. 0 = Waage mittig, 1 = Waage links, 2 = Waage rechts

	// # Erstellen der Gewichte als Bilder + der Ablageflaechen
	var gewichteArray = new Array();
	var ablageboxArray = new Array();

	for (i=0; i < anzahlG; i++){

		// Array fuer die Gewichte
		var j = position.pop();
		gewichteArray[j] = new Array(); // Mehrdimensionales Array, um meherere Infos zu den Gewichten zu speichern
		gewichteArray[j]["x"] = posXminG + (j * deltaposG);
		gewichteArray[j]["h"] = hoeheG;
		gewichteArray[j]["y"] = posYG;
		gewichteArray[j]["b"] = breiteG;

		var containerGewicht = new createjs.Container();
		containerGewicht.x = gewichteArray[j]["x"];
		containerGewicht.y = gewichteArray[j]["y"];
		containerGewicht.setBounds(containerGewicht.x, containerGewicht.y, breiteG, hoeheG);
		containerGewicht["i"] = i;
		gewichteArray[j]["shape"] = containerGewicht;

		var gew = new createjs.Bitmap("img/"+(2*i+3)+".png");
		gew.scaleX = skB(1);
		gew.scaleY = skH(1);
		containerGewicht["gew"] = gew;
		containerGewicht.addChild(gew);

		// Array fuer die Ablagekaesten
		ablageboxArray[j]=new Array();
		boxshape = new createjs.Shape();
		boxshape.graphics.beginStroke("grey").drawRect(0,0,breiteG, hoeheG);
		boxshape.x = posXminG + (j * deltaposG);
		boxshape.y = posYG;
		boxshape.setBounds(boxshape.x, boxshape.y, breiteG, hoeheG);
		ablageboxArray[j]["boxshape"] = boxshape;
		stage.addChild(ablageboxArray[j]["boxshape"]);
		stage.addChild(gewichteArray[j]["shape"]);

		stage.update();
	}

	// Erstellen der Boxen, die die Gewichte verdecken
	for(i=0; i<anzahlG; i++) {
		var box = new createjs.Bitmap("img/BOX"+(i+1)+".png");
		box.alpha=0;
		box.scaleX = skB(1);
		box.scaleY = skH(1);
		gewichteArray[i]["shape"]["bo"] = box;
		gewichteArray[i]["shape"].addChild(box);
		createjs.Tween.get(box).to({y:-skH(250)}, 500, createjs.Ease.linear);
	}
	// gewichte werden nur kurz gezeigt und dann verdeckt
	setTimeout(verdecken, 500);

	//Waage Box 1
	var box1 = new createjs.Shape(); // Waage-Box links
	box1.graphics.drawRect(0,0, breiteW, hoeheW);
	box1.x = posXW1;
	box1.y = posYW1;
	box1["filled"] = false;
	var inbox1=0;
	box1.setBounds(box1.x, box1.y, breiteW, hoeheW);
	stage.addChild(box1);

	//Waage Box 2
	var box2 = new createjs.Shape(); // Waage-Box rechts
	box2.graphics.drawRect(0,0, breiteW, hoeheW);
	box2.x = posXW2;
	box2.y = posYW2;
	box2["filled"] = false;
	var inbox2 = 0;
	box2.setBounds(box2.x, box2.y, breiteW, hoeheW);
	stage.addChild(box2);

	// # Kreuz: bei falschem Loesen blinkt es auf
	var kreuz = new createjs.Bitmap("img/kreuz.png");
	kreuz.alpha = 0;
	kreuz.scaleX = skB(1);
	kreuz.scaleY = skH(1);
	kreuz.x = skB(670);
	kreuz.y = skH(200);
	stage.addChild(kreuz);
	stage.update();

	// ------------------------------------ Methoden Teil --------------------------------------------------------

	//Alle Elemente können bewegt werden
	//forEach geht jedes element im Array durch, dass durch den Schlüssel assoziiert wird
	gewichteArray.forEach(function(element, index) {
		//Bewegen jedes shapes im Array
        element["shape"].on("pressmove", function (evt) {
			createjs.Tween.removeTweens(element["shape"]);
			window.timeout = window.timeoutvar;
			evt.currentTarget.x = evt.stageX - (element["b"]/2); //Mitte shapes
			evt.currentTarget.y = evt.stageY - (element["h"]/2); //Mitte shapes
			stage.update();
		});
	});

	//einrasten der Shapes in die jeweiligen Boxen
	gewichteArray.forEach(function(element, index) {
		element["shape"].on("pressup", function (evt){
			window.timeout = window.timeoutvar;
				//Box1
				//Einraste wenn leer
				if(intersect(evt.currentTarget, box1) && box1["filled"] == false){
					anzahlW++;
					element["shape"].x = box1.x + (breiteW-element["b"])/2;
					element["shape"].y = box1.y + (hoeheW-element["h"])/2;
					box1["filled"] = true;
				//Wenn belegt werfe raus
				} else if(intersect(evt.currentTarget, box1) && box1["filled"] == true){
					element["shape"].x = element["shape"].x - skB(200)*Math.random() - 100;
				}
				//Box2
				//Einraste wenn leer
				if(intersect(evt.currentTarget, box2) && box2["filled"] == false){
					anzahlW++;
					element["shape"].x = box2.x + (breiteW-element["b"])/2;
					element["shape"].y = box2.y + (hoeheW-element["h"])/2;
					box2["filled"] = true;
				//Wenn belegt werfe raus
				}else if(intersect(evt.currentTarget, box2) && box2["filled"] == true){
					element["shape"].x = element["shape"].x + skB(200)*Math.random() + 100;
				}
				//Ablage Boxen
				//Wenn welche "welcheablage" null ist, sind wir über keiner box, dann tue nichts
				if (welcheablage(ablageboxArray, evt.currentTarget) != null){
					//Wenn wir über einer Ablage sind, überprüfe diese Ablage wieviele shapes drin sind.
					//Wenn kein zusätzliches shape, setzte rein
					if(shapesinablage(welcheablage(ablageboxArray, evt.currentTarget), gewichteArray) < 2){
						element["shape"].x = welcheablage(ablageboxArray, evt.currentTarget).x;
						element["shape"].y = welcheablage(ablageboxArray, evt.currentTarget).y;
						console.log("würde gewichte reinsetzen");
					}
					//sonst schmeiße raus
					else if(shapesinablage(welcheablage(ablageboxArray, evt.currentTarget), gewichteArray) > 1){
						element["shape"].y = posYG + skH(250);
						console.log("gewicht nach unten raus");
					}
				}
			zustandWaageAendern();
		});
    });

	//Ausschlagen der Waage
	function zustandWaageAendern(){
		switch(ausschlagen(gewichteArray, box1, box2)){
			//links beladen, rechts leer
			case 0:
				alleTiereWeg();
				createjs.Tween.get(box1, {loop:false}).to({y:tiltmin}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(box2, {loop:false}).to({y:tiltmax}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(inbox(box1, gewichteArray), {loop:false}).to({y:tiltmin}, tilttime, createjs.Ease.linear);
				box1.setBounds(box1.x, tiltmax, breiteW, hoeheW);
				box2.setBounds(box2.x, tiltmin, breiteW, hoeheW);
				animationF(1); //nach links ausschlagen
				box2["filled"] = false;
				break;
			//rechts beladen, links leer
			case 1:
				alleTiereWeg();
				createjs.Tween.get(box1, {loop:false}).to({y:tiltmax}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(box2, {loop:false}).to({y:tiltmin}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(inbox(box2, gewichteArray), {loop:false}).to({y:tiltmin}, tilttime, createjs.Ease.linear);
				box1.setBounds(box1.x, tiltmin, breiteW, hoeheW);
				box2.setBounds(box2.x, tiltmax, breiteW, hoeheW);
				animationF(2); //nach rechts ausschlagen
				box1["filled"] = false;
				break;

			//beide beladen
			case 2:
				//links schwerer als rechts
				if (inbox(box1, gewichteArray)["i"] > inbox(box2, gewichteArray)["i"]){
					createjs.Tween.get(box1, {loop:false}).to({y:tiltmin}, tilttime, createjs.Ease.linear);
					createjs.Tween.get(box2, {loop:false}).to({y:tiltmax}, tilttime, createjs.Ease.linear);
					createjs.Tween.get(inbox(box1, gewichteArray), {loop:false}).to({y:tiltmin}, tilttime, createjs.Ease.linear);
					createjs.Tween.get(inbox(box2, gewichteArray), {loop:false}).to({y:tiltmax}, tilttime, createjs.Ease.linear);
					box1.setBounds(box1.x, tiltmax, breiteW, hoeheW);
					box2.setBounds(box2.x, tiltmin, breiteW, hoeheW);
					animationF(1); //nach links ausschlagen
					elefant1.alpha = 1;
					vogel2.alpha = 1;

					proof = true;
				//rechts schwerer als links
				}else{
					createjs.Tween.get(box1, {loop:false}).to({y:tiltmax}, tilttime, createjs.Ease.linear);
					createjs.Tween.get(box2, {loop:false}).to({y:tiltmin}, tilttime, createjs.Ease.linear);
					createjs.Tween.get(inbox(box1, gewichteArray), {loop:false}).to({y:tiltmax}, tilttime, createjs.Ease.linear);
					createjs.Tween.get(inbox(box2, gewichteArray), {loop:false}).to({y:tiltmin}, tilttime, createjs.Ease.linear);
					box1.setBounds(box1.x, tiltmin, breiteW, hoeheW);
					box2.setBounds(box2.x, tiltmax, breiteW, hoeheW);
					animationF(2); //nach rechts ausschlagen
					elefant2.alpha = 1;
					vogel1.alpha = 1;
					proof = true;
				}
				break;
			//beide leer
			case 3:
				alleTiereWeg();
				createjs.Tween.get(box1, {loop:false}).to({y:tilt0}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(box2, {loop:false}).to({y:tilt0}, tilttime, createjs.Ease.linear);
				box1.setBounds(box1.x, tilt0, breiteW, hoeheW);
				box2.setBounds(box2.x, tilt0, breiteW, hoeheW);
				animationF(0); //in die Mitte zurueck
				box1["filled"] = false;
				box2["filled"] = false;
				break;
		}
	}

	// Zurueck zum Intro
	function clickZurueck(event){
		window.timeout = window.timeoutvar;
		stage.removeAllChildren();
		stage.removeAllEventListeners();

		intro(); // Zurueck zum Einfuehrungsvideo
	}

	// die Gewichte werden von den Boxen verdeckt
	function verdecken() {
		for(i=0; i<anzahlG; i++) {
			var aktbox = gewichteArray[i]["shape"]["bo"];
			aktbox.alpha=1;
		}
		setTimeout(runter, 1000);
	}

	//Loesen Funktion. Wenn der Button gedrueckt wird, soll entschieden werden, ob alle Gewichte richtig sortiert sind.
	function clickLoesen(event) {
		var erg = true;
		for (i=0; i<anzahlG; i++) {
			var aktBox = ablageboxArray[i]["boxshape"];
			var shapeDort = inbox(aktBox, gewichteArray);

			// Alle Gewichte IN den Ablageboxen werden ueberprueft. Fehlt eins, oder liegt eins falsch: erg = false
			if(shapeDort==null || shapeDort["i"] != i) {
				erg=false;
			}
		}
		if(erg) {
			// die Boxen werden hochgefahren, damit die sortierte Liste sichtbar ist
			for(i=0; i<anzahlG; i++) {
				var aktbo =gewichteArray[i]["shape"]["bo"];
				createjs.Tween.get(aktbo).to({y:-skH(250)}, 500, createjs.Ease.linear);
			}
			setTimeout(clickWeiter, 2000);
		}
		else {
			// Ein Kreuz blinkt auf - weiter machen
			kreuz.alpha=1;
			setTimeout(function(){ kreuz.alpha=0; }, 300);
			setTimeout(function(){ kreuz.alpha=1; }, 600);
			setTimeout(function(){ kreuz.alpha=0; }, 1700);
		}
	}

	// Auf die naechste Stage
	function clickWeiter() {
		stage.removeAllChildren();
		stage.removeAllEventListeners();
		modus();
	}

	// Boxen werden ueber die Gewichte gesetzt
	function runter() {
		for(i=0; i<anzahlG; i++) {
			var aktbo =gewichteArray[i]["shape"]["bo"];
			createjs.Tween.get(aktbo).to({y:0}, 500, createjs.Ease.linear);
		}
	}

	// Alle Tiere neben der Waage wegmachen
	function alleTiereWeg () {
		elefant1.alpha = 0;
		elefant2.alpha = 0;
		vogel1.alpha = 0;
		vogel2.alpha = 0;
	}

	// Die Funktion bekommt uebergeben in welche Richtung die Waage ausschlagen soll und ueberfrueft,
	// welche Animation dafuer abgespielt werden muss und setzt den Status der Waage neu
	// 0 = Mitte, 1 = Links, 2 = Rechts, animationStatus = aktuelle Position der Waage
	function animationF(richtung){
		switch(richtung) {
			case 0: if(animationStatus==1) animationW.gotoAndPlay("linksR");
					else if(animationStatus==2) animationW.gotoAndPlay("rechtsR");
					animationStatus=0;
					break;

			case 1: if(animationStatus==0) animationW.gotoAndPlay("links");
					else if(animationStatus==2) {
						animationW.gotoAndPlay("rechtsR");
						animationW.gotoAndPlay("links");
					}
					animationStatus=1;
					break;
			case 2: if(animationStatus==0) animationW.gotoAndPlay("rechts");
					else if(animationStatus==1) {
						animationW.gotoAndPlay("linksR");
						animationW.gotoAndPlay("rechts");
					}
					animationStatus=2;
					break;
		}
	}
}
