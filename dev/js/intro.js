function intro(){

	stage.removeAllEventListeners();
	stage.removeAllChildren();

	// ------------------------------------------- Objekte hinzufuegen --------------------------------------------
	createjs.Ticker.addEventListener("tick", erneuern);

	// Buttons:
	// # Zurueck Button
	var buttonZurueck = new createjs.Shape();
	buttonZurueck.graphics.beginFill("red").drawRoundRect(0,0,skB(buttonBreite),skH(buttonHoehe),skB(20));
	buttonZurueck.shadow = new createjs.Shadow("#000000", 5,5,skB(5));

	var bildZurueck = new createjs.Bitmap("img/pfeillinks.png");
	bildZurueck.scaleX = skB(1);
	bildZurueck.scaleY = skH(1);

	var containerZurueck = new createjs.Container();
	containerZurueck.x = skB(30);
	containerZurueck.y = skH(1080 - buttonHoehe - 30);
	containerZurueck.addChild(buttonZurueck, bildZurueck);
	containerZurueck.addEventListener("click", clickZurueck);

	stage.addChild(containerZurueck);

	// # Weiter Button
	var buttonWeiter = new createjs.Shape();
	buttonWeiter.graphics.beginFill("#04B404").drawRoundRect(0,0,skB(buttonBreite),skH(buttonHoehe),skB(20));

	var bildWeiter = new createjs.Bitmap("img/pfeilrechts.png")
	bildWeiter.scaleX = skB(1);
	bildWeiter.scaleY = skH(1);

	var containerWeiter = new createjs.Container();
	containerWeiter.x = skB(1920 - buttonBreite - 30);
	containerWeiter.y = skH(1080 - buttonHoehe - 30);
	containerWeiter.addChild(buttonWeiter, bildWeiter);
	containerWeiter.addEventListener("click", clickWeiter);

	stage.addChild(containerWeiter);

	// # Einfuehrungsvideo
	var video = MTLG.assets.getAsset('img/aufnahme2.mp4')
	video.currentTime = 0;
	video.pause();
	var videoBitmap = new createjs.Bitmap(video);
	videoBitmap.scaleX = skB(1920/1280); // Groesse des Videos ist 1280 x 720
	videoBitmap.scaleY = skH(1080/720);
	stage.addChild(videoBitmap);

	// # Bild von einer Kamera
	var kamera = new createjs.Bitmap("img/camera.png");
	kamera.scaleX = skB(1);
	kamera.scaleY = skH(1);
	stage.addChild(kamera);

	abspielen();

	// ----------------------------------- Methoden Teil ------------------------------------------------

	// # Video wird ab 0 abgespielt
	function abspielen() {
		blinken();

		video.currentTime = 0;
		video.play();
		video.addEventListener('ended', clickWeiter, false); // bei Ende wird die neue Stage aufgerufen
	}

	// # Das Symbol der Kamera blinkt
	function blinken() {

		setTimeout(function(){ kamera.alpha=0; }, 1000);
		setTimeout(function(){ kamera.alpha=1; }, 2000);
		setTimeout(function(){ kamera.alpha=0; }, 3000);
		setTimeout(function(){ kamera.alpha=1; }, 4000);
		setTimeout(function(){ kamera.alpha=0; }, 5000);
		setTimeout(function(){ kamera.alpha=1; }, 6000);

	}

	// # Der Timeout wird waehrend des Videos nicht weitergezaehlt
	function erneuern() {
		window.timeout = window.timeoutvar;
	}

	// # Weiter zur Einfuehrung (ueberspringen)
	function clickWeiter(){
		window.timeout = window.timeoutvar;

		video.pause();
		createjs.Ticker.removeEventListener("tick", erneuern);

		einfuehrung();
	}

	// # Zurueck zur Spielerauswahl
	function clickZurueck(event){
		location.reload()
	}
}
