//Funktion überprüft ob in den boxen gewichte liegen und gibt die 4 möglichen Fälle zurück
//Bekommt ein Array von gewichten und 2 Box shapes übergeben.
function ausschlagen (GewichteArray, box1, box2){
//	console.log("Inbox1 Wert:" + inbox(box1, gewicht))
//	console.log("Inbox2 Wert:" + inbox(box2, gewicht))
	//links beladen, rechts leer
	if(inbox(box1, GewichteArray) != null && inbox(box2, GewichteArray) == null){
		return 0;
	}
	//rechts beladen, links leer
	if(inbox(box1, GewichteArray) == null && inbox(box2, GewichteArray) != null){
		return 1;
	}
	//beide beladen
	if(inbox(box1, GewichteArray) != null && inbox(box2, GewichteArray) != null){
		return 2;
	}
	//beide leer
	if(inbox(box1, GewichteArray) == null && inbox(box2, GewichteArray) == null){
		return 3;
	}
}

//Funktion gibt gewicht shape zurück, der in einer box liegt.
//Bekommt ein Array von Gewichten übergeben und eine Box
function inbox(box, GewichteArray){
	var shape = null;
	GewichteArray.forEach(function(element, index) {
		if(intersect(box, element["shape"])){
			shape = element["shape"];
		}
	});
//	console.log("inbox shape geworfen");
	return shape;
}

//Funktion gib ein Array mit allen Shapes zurück, die in einer Ablagebox liegen.
//Bekommt eine Box und ein Array von Gewichten
function shapesinablage(box, GewichteArray){
	var boxreturn = [];
	GewichteArray.forEach(function(element, index) {
		if(intersect(element["shape"], box)){
			boxreturn.push(element["shape"]);
		}
	});
//	console.log("shapesinablage geworfen");
	console.log("shapes in ablage: " + boxreturn.length);
	return boxreturn.length;
}

//Funktion gibt den shape der Ablage box zurück über dem das gewicht ist
//Bekommt ein Array  von Boxen und ein Gewicht shape übergeben
function welcheablage(BoxArray, gewicht){
	var boxreturn = null;
	BoxArray.forEach(function(element, index) {
		if(intersect(gewicht, element["boxshape"])){
			boxreturn = element["boxshape"];
		}
	});
	console.log("welcheablage shape geworfen");
	return boxreturn;
}
//Funktion ordnet Elemente in einem Array zufällig neu an
//Bekommt ein Array übergeben
function shuffle(array) {
	var currentIndex = array.length;
	var temporaryValue;
	var randomIndex;

	while (0 !== currentIndex) {

		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}
	return array;
}


//überprüfung ob ein Element innerhalb eines Kastens ist
function intersect(obj, box) {

	var objBounds = obj.getBounds();
	var boxBounds = box.getBounds();
	if(obj.parent)
		var point = obj.parent.localToLocal(obj.x, obj.y, box); // Ecke oben links des Objektes
	else
		var point = obj.globalToLocal(obj.x, obj.y, box); // Ecke oben links des Objektes
	//Ueberpruefung, ob das gesamte Objekt links oder rechts der box ist
	if(point.x > boxBounds.width || point.x + objBounds.width < 0) {
		return false;
	}
	// Ueberpruefung, ob das gesamte Objekt ueber oder unter der box ist
	if(point.y > boxBounds.height || point.y + objBounds.height < 0) {
		return false;
	}
	//Wird nur im Fall horizontal + vertikal in der box erreicht
	return true;
}

// Skalierung
function skB(x) { // in der Breite
		return fensterBreite/1920 * x;
	}

function skH(y) { // in der Hoehe
	return fensterHoehe/1080 * y;
}
