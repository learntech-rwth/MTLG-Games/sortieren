function spielerauswahl() {

	stage.removeAllEventListeners();
	stage.removeAllChildren();

	// Globale Variablen
	// # Speichert die Anzahl der Spieler
	window.anzahlSpieler;

	// # Speicherung der Fensterbreite zu Skalierungszwecken
	window.fensterBreite = stage.canvas.width;
	window.fensterHoehe = stage.canvas.height;

	// # Speicherung der Buttongroeße - ueberall gleich!
	window.buttonBreite = 180;
	window.buttonHoehe = 100;

	// ------------------------------------- Objekte werden zur Stage hinzugefuegt ---------------------------------------------

	// # Hintergrund
	var hintergrund = new createjs.Bitmap("img/hintergrund.png");
	hintergrund.scaleX = skB(1);
	hintergrund.scaleY = skH(1);
	stage.addChild(hintergrund);

	// # Zielfeld
	var breiteZ = 900;
	var hoeheZ = 400;
	var posXZ = (fensterBreite - breiteZ)/3 + 120;
	var posYZ = (fensterHoehe - hoeheZ)/3;

	var boxZ = new createjs.Shape();
	boxZ.x = posXZ;
	boxZ.y = posYZ;
	boxZ.graphics.beginStroke("grey").drawRect(0, 0, skB(breiteZ), skH(hoeheZ));
	boxZ.setBounds(0, 0, skB(breiteZ), skH(hoeheZ));

	stage.addChild(boxZ);

	// # Start Button
	var breiteS = 360;
	var hoeheS = 200;

	var buttonStart = new createjs.Shape();
	buttonStart.graphics.beginFill("#04B404").drawRoundRect(0,0, breiteS, hoeheS,skB(20));
	skalierung(buttonStart, breiteS, hoeheS, 0, 0);

	var bildStart = new createjs.Bitmap("img/pfeilrechts.png");
	bildStart.scaleX = skB(2);
	bildStart.scaleY = skH(2);

	var containerStart = new createjs.Container();
	skalierung(containerStart, breiteS, hoeheS, 1400, 450); // Setzt die Position und skaliert
	containerStart.addChild(buttonStart, bildStart);
	containerStart.alpha = 0.3;
	containerStart.addEventListener("click", clickStart);
	stage.addChild(containerStart);

	// # Wischhand (Symbol)
	var hand = new createjs.Bitmap("img/hand.png");
	hand.scaleX = skB(1); // hand.breite = 200
	hand.scaleY = skH(1); // hand.hoehe = 300
	hand.x = skB(50);
	hand.y = skH((1080 - 300)/2);
	stage.addChild(hand);

	// # Spielfiguren

	// wie viele Figuren wurden ausgewaehlt?
	var gewaehlteFiguren = 0;
	// Hoehe und Breite der Figuren (aufgrund der Bilder)
	var breiteF = 200;
	var hoeheF = 321;
	var diff = (breiteZ - 4*breiteF)/3;

	// Figuren erstellen
	var graueFigurenArray = new Array();
	var FigurenArray = new Array();

	for (i=0; i < 4; i++) {
		// ausgegraute Figuren in der Zielflaeche
		var grau = new createjs.Bitmap("img/figur_"+(i+1)+"GRAU.png");
		grau.scaleX = skB(0.9);
		grau.scaleY = skH(0.9);
		grau.alpha = 0.5;
		grau.x = skB(posXZ + i*(breiteF + diff));
		grau.y = skH(posYZ) + (hoeheZ-hoeheF)/2;
		graueFigurenArray[i] = grau;
		stage.addChild(graueFigurenArray[i]);

		// Spielfiguren
		FigurenArray[i] = new Array();
		var bild = new createjs.Bitmap("img/figur_"+(i+1)+".png");
		bild.scaleX = skB(0.9);
		bild.scaleY = skH(0.9);
		FigurenArray[i]["bild"] = bild;

		var spieler = new createjs.Container();
		spieler.x = skB(posXZ + i*(breiteF + diff));
		spieler.y = skH(posYZ + hoeheZ + 50);
		spieler.addChild(bild);
		FigurenArray[i]["spieler"] = spieler;

		FigurenArray[i]["gewaehlt"] = false;
		FigurenArray[i]["index"] = i;

		stage.addChild(FigurenArray[i]["spieler"]);
	}

	// ------------------------------------- Methoden Teil ----------------------------------

	// # Spielfiguren koennen sich bewegen
	FigurenArray.forEach(function(element, index) {
		element["spieler"].on("pressmove", function (evt) {

			window.timeout = window.timeoutvar;

			// Element bewegt sich mit der Maus mit
			evt.currentTarget.x = evt.stageX - (skB(breiteF)/2); //Mitte des Bildes
			evt.currentTarget.y = evt.stageY - (skH(hoeheF)/2); //Mitte des Bildes

			// Die Zielbox verfaerbt sich: so wird klar, wann die Figuren reingezogen werden, und wann nicht
			if(intersect(evt.currentTarget, boxZ) ){
				boxZ.graphics.beginStroke("black").drawRect(0, 0, skB(breiteZ), skH(hoeheZ));
			}else{
				boxZ.graphics.beginStroke("grey").drawRect(0, 0, skB(breiteZ), skH(hoeheZ));
			}
		});
	});

	// # Spielfiguren koennen abgelegt werden
	FigurenArray.forEach(function(element, index) {
		element["spieler"].on("pressup", function (evt){

			window.timeout = window.timeoutvar;

			if(intersect(evt.currentTarget, boxZ)){
				// Figur wird an ihren Platz IM Zielfeld gezogen
				element["spieler"].x = skB(posXZ + index*(breiteF + diff));
				element["spieler"].y = skH(posYZ) + (hoeheZ-hoeheF)/2;
				boxZ.graphics.beginStroke("grey").rect(0, 0, skB(breiteZ), skH(hoeheZ));

				// Die Anzahl der aktuell gewaehlten Figuren wird bestimmt
				if(!element["gewaehlt"]) {
					gewaehlteFiguren++;
					element["gewaehlt"] = true;
					buttonStart.shadow = new createjs.Shadow("#000000", 5, 5, 5);
					containerStart.alpha=1;
				}
			}
			else {
				// Figur wird an ihren Platz AUSSERHALB des Zielfeldes gezogen
				element["spieler"].x = skB(posXZ + index*(breiteF + diff));
				element["spieler"].y = skH(posYZ + hoeheZ + 50);

				// Figur ist nicht mehr gewaehlt - falls keine wird der Startbutton grau
				if(element["gewaehlt"]){
					gewaehlteFiguren--;
					element["gewaehlt"]=false;
				}
				if(gewaehlteFiguren==0) {
					buttonStart.shadow = new createjs.Shadow("#000000", 0, 0, 0);
					containerStart.alpha = 0.3;
				}
			}
		});
	});

	// # Click Event fuer den Start Button
	function clickStart(event) {
		window.timeout = window.timeoutvar;

		if(gewaehlteFiguren>0) {
			anzahlSpieler = gewaehlteFiguren;
			intro();
			// modus(); //Wenn bei der Entwicklung das Intro übersprungen werden soll.
		}
	}

	// # Skalierungsfunktion
	function skalierung(objekt, b, h, xK, yK) { // Skalierung eines ganzen Objektes
		objekt.x = skB(xK);
		objekt.y = skH(yK);
		objekt.scaleX =skB(1);
		objekt.scaleY = skH(1);
		objekt.setBounds(0, 0, skB(b), skH(h));
	}

	//------------------------------- Timeout -------------------------------------------------------------------

	window.timeoutvar = 120; // aktuell bei 120 Sekunden
	window.timeout = window.timeoutvar;

	var interval_id = window.setInterval(timeout, 1000);

	function timeout(event){
		var temptime = window.timeout;
		window.timeout = temptime - 1;
		if (window.timeout < 1){
			location.reload();
		}
	}
}
