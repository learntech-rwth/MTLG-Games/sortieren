function modus(){

	stage.removeAllEventListeners();
	stage.removeAllChildren();

	// ---------------------------------------- Objekte hinzufuegen ----------------------------------------------

	// # Hintergrund
	var hintergrund = new createjs.Bitmap("img/hintergrund.png");
	hintergrund.scaleX = skB(1);
	hintergrund.scaleY = skH(1);
	stage.addChild(hintergrund);

	// Buttons:
	// # Zurueck Button
	var buttonZurueck = new createjs.Shape();
	buttonZurueck.graphics.beginFill("red").drawRoundRect(0,0,skB(buttonBreite),skH(buttonHoehe),skB(20));
	buttonZurueck.shadow = new createjs.Shadow("#000000", 5,5,skB(5));

	var bildZurueck = new createjs.Bitmap("img/pfeillinks.png");
	bildZurueck.scaleX = skB(1);
	bildZurueck.scaleY = skH(1);

	var containerZurueck = new createjs.Container();
	containerZurueck.x = skB(30);
	containerZurueck.y = skH(1080 - buttonHoehe - 30);
	containerZurueck.addChild(buttonZurueck, bildZurueck);
	containerZurueck.addEventListener("click", clickZurueck);

	stage.addChild(containerZurueck);

	// # Weiter Button
	var buttonWeiter = new createjs.Shape();
	buttonWeiter.graphics.beginFill("#04B404").drawRoundRect(0,0,skB(buttonBreite),skH(buttonHoehe),skB(20));

	var bildWeiter = new createjs.Bitmap("img/pfeilrechts.png")
	bildWeiter.scaleX = skB(1);
	bildWeiter.scaleY = skH(1);

	var containerWeiter = new createjs.Container();
	containerWeiter.x = skB(1920 - buttonBreite - 30);
	containerWeiter.y = skH(1080 - buttonHoehe - 30);
	containerWeiter.alpha = 0.3;
	containerWeiter.addChild(buttonWeiter, bildWeiter);

	stage.addChild(containerWeiter);

	// # Variablen zur Auswahlbutton Erstellung
	var posXKasten = 570;
	var posYKasten = 200;
	var breiteKasten = 1000;
	var hoeheKasten = 600;

	var posXLevel = 270;
	var posYLevel = 200;
	var breiteLevel = 200;
	var hoeheLevel = 100;
	var diffLevel = (hoeheKasten - 3* 100)/2;

	// # Erstellung der Auswahlbuttons
	var LevelArray = new Array();

	for (i=0; i<3; i++) {
		LevelArray[i] = new Array();

		var buttonLevel = new createjs.Shape();
		buttonLevel.graphics.beginFill("#0B3861").drawRoundRect(0,0,skB(breiteLevel),skH(hoeheLevel),skB(20));

		var bildLevel = new createjs.Bitmap("img/Level"+(i+1)+".png");
		bildLevel.scaleX = skB(1);
		bildLevel.scaleY = skH(1);

		var containerLevel = new createjs.Container();
		containerLevel.x = skB(posXLevel);
		containerLevel.y = skH(posYLevel + i*(100 + diffLevel));
		containerLevel.addChild(buttonLevel, bildLevel);
		containerLevel.shadow = new createjs.Shadow("#000000", 5,5,skB(5));

		LevelArray[i] = containerLevel;
		LevelArray[i]["button"] = buttonLevel;
		stage.addChild(LevelArray[i]);
	}

	// ------------------------------------------ Methoden Teil ------------------------------------------------

	// # Click Funktionen fuer die Auswahlbuttons

	LevelArray.forEach(function(element, index) {
		element["button"].addEventListener("click", function (event) {

			window.timeout = window.timeoutvar;

			// Da eine Auswahl besteht, kann man weiter klicken
			buttonWeiter.shadow = new createjs.Shadow("#000000", 5,5,skB(5));
			containerWeiter.alpha = 1;
			containerWeiter.addEventListener("click", clickWeiter);

			// Buttons einfaerben, um die Auswahl anzuzeigen
			for(i=0; i<3; i++) {
				LevelArray[i]["button"].graphics.beginFill("#0B3861")
									   .drawRoundRect(0,0,skB(breiteLevel),skH(hoeheLevel),skB(20));
			} // Nur das gewaehlte wird gruen
			element["button"].graphics.beginFill("#04B404")
							 .drawRoundRect(0,0,skB(breiteLevel),skH(hoeheLevel),skB(20));

			// Kasten wird erstellt, der den Modus anzeigt
			var kasten = new createjs.Shape();
			kasten.graphics.beginFill("#04B404").drawRoundRect(0,0,skB(breiteKasten),skH(hoeheKasten),skB(20));

			var bildKasten = new createjs.Bitmap("img/modus-"+ (index+1) + ".png");
			bildKasten.scaleX = skB(1);
			bildKasten.scaleY = skH(1);

			var containerKasten = new createjs.Container();
			containerKasten.x = skB(posXKasten);
			containerKasten.y = skH(posYKasten);
			containerKasten.addChild(kasten, bildKasten);

			stage.addChild(containerKasten);

			// Der ausgewaehlte Modus wird fuer die folgenden Stages (global) abgespeichert
			modusWaehlen(index);
		});
	});

	// # Der Modus wird anhand des Index festgelegt
	function modusWaehlen(index) {
		window.help = false;
		window.bubble = false;
		if (index == 0) window.help = true;
		if (index == 1) window.bubble = true;
	}

	// # Zurueck zur Einfuehrung
	function clickZurueck(event){
		window.timeout = window.timeoutvar;

		einfuehrung();
	}

	// # Die Funktion wird von dem Weiter Button aufgerufen
	function clickWeiter(){
		window.timeout = window.timeoutvar;

		sgrad();
	}

}
