function sgrad(){

	stage.removeAllEventListeners();
	stage.removeAllChildren();

	// globale Variablen fuer den Modus
	window.tempZaehler = false;
	window.anzg = 4;

	// -------------------------------------- Objekte hinzufuegen -------------------------------------------------
	// # Hintergrund
	var hintergrund = new createjs.Bitmap("img/hintergrund.png");
	hintergrund.scaleX = skB(1);
	hintergrund.scaleY = skH(1);
	stage.addChild(hintergrund);

	// Buttons hinzufuegen
	// # Zurueck Button
	var buttonZurueck = new createjs.Shape();
	buttonZurueck.graphics.beginFill("red").drawRoundRect(0,0,skB(buttonBreite),skH(buttonHoehe),skB(20));
	buttonZurueck.shadow = new createjs.Shadow("#000000", 5,5,skB(5));

	var bildZurueck = new createjs.Bitmap("img/pfeillinks.png");
	bildZurueck.scaleX = skB(1);
	bildZurueck.scaleY = skH(1);

	var containerZurueck = new createjs.Container();
	containerZurueck.x = skB(30);
	containerZurueck.y = skH(1080 - buttonHoehe - 30);
	containerZurueck.addChild(buttonZurueck, bildZurueck);
	containerZurueck.addEventListener("click", clickZurueck);

	stage.addChild(containerZurueck);

	// # Weiter Button

	var buttonWeiter = new createjs.Shape();
	buttonWeiter.graphics.beginFill("#04B404").drawRoundRect(0,0,skB(buttonBreite),skH(buttonHoehe),skB(20));

	var bildWeiter = new createjs.Bitmap("img/pfeilrechts.png")
	bildWeiter.scaleX = skB(1);
	bildWeiter.scaleY = skH(1);

	var containerWeiter = new createjs.Container();
	containerWeiter.x = skB(1920 - buttonBreite - 30);
	containerWeiter.y = skH(1080 - buttonHoehe - 30);
	containerWeiter.alpha = 0.3;
	containerWeiter.addChild(buttonWeiter, bildWeiter);

	stage.addChild(containerWeiter);

	// # Buttons fuer den Schwierigkeitsgrad
	var buttonH = 270/2;
	var diffH = (1080 - 4*buttonH)/8;
	var xButtons = 400;
	var yButtons = 2*diffH;
	// Die Buttons haben - abhaengig von den Bildern - unterschiedliche Breiten
	var BreitenArray = [600/2, 830/2, 1100/2];

	// leicht, mittel und schwer
	var ModusArray = new Array();
	for (i=0; i<3; i++) {
		ModusArray[i] = new Array();

		var modusButton = new createjs.Shape();
		modusButton.graphics.beginFill("#0B3861").drawRoundRect(0,0,skB(BreitenArray[i]),skH(buttonH),skB(20));

		var bildModus = new createjs.Bitmap("img/schwer"+(i+1)+".png");
		bildModus.scaleX = skB(0.5);
		bildModus.scaleY = skH(0.5);

		var containerModus = new createjs.Container();
		containerModus.x = skB(xButtons + i*100);
		containerModus.y = skH(yButtons + i*(buttonH + diffH));
		containerModus.addChild(modusButton, bildModus);

		ModusArray[i] = containerModus;
		ModusArray[i]["button"] = modusButton;
		stage.addChild(ModusArray[i]);
	}
	// Wettkampf
	var buttonWettkampf = new createjs.Shape();
	buttonWettkampf.graphics.beginFill("orange").drawRoundRect(0,0,skB(1040/2),skH(buttonH),skB(20));

	var textWettkampf = new createjs.Bitmap("img/wettkampf.png");
	textWettkampf.scaleX = skB(0.5);
	textWettkampf.scaleY = skH(0.5);

	var containerWettkampf = new createjs.Container();
	containerWettkampf.x = skB(xButtons + 500);
	containerWettkampf.y = skH(yButtons + 3*buttonH + 3*diffH);
	containerWettkampf.addChild(buttonWettkampf, textWettkampf);

	stage.addChild(containerWettkampf);

	containerWettkampf.addEventListener("click", clickWettkampf);

	// -------------------------------------- Methoden Teil ------------------------------------------------------

	// # Buttons koennen angeklickt werden
	ModusArray.forEach(function(element, index) {
		element["button"].addEventListener("click", function (event) {

			window.timeout = window.timeoutvar;

			// Die Auswahl wird durch Farbe angezeigt und abgespeichert
			faerben(index);
			anzahlSpeichern(index);

			buttonWeiter.shadow = new createjs.Shadow("#000000", 5,5,skB(5));
			containerWeiter.alpha=1;
			containerWeiter.addEventListener("click", clickWeiter);
		});
	});

	// # Wettkampf Button kann angeklickt werden
	function clickWettkampf(event){
		window.timeout = window.timeoutvar;

		// Die Auswahl wird durch Farbe angezeigt und abgespeichert
		faerben(3);
		anzahlSpeichern(3);

		buttonWeiter.shadow = new createjs.Shadow("#000000", 5,5,skB(5));
		containerWeiter.alpha=1;
		containerWeiter.addEventListener("click", clickWeiter);
	}

	// # Faerbt den ausgewaehlten Button gruen, alle anderen normal
	function faerben(index) {
		for (j=0; j<3; j++) {
			ModusArray[j].shadow = new createjs.Shadow("#000000", 0,0,0);
			ModusArray[j]["button"].graphics.beginFill("#0B3861").drawRoundRect(0,0,skB(BreitenArray[j]),skH(buttonH),skB(20));
			containerWettkampf.shadow = new createjs.Shadow("#000000", 0,0,0);
			buttonWettkampf.graphics.beginFill("orange").drawRoundRect(0,0,skB(1040/2),skH(buttonH),skB(20));
		}

		// ausgewaehlter Button:

		if(index<3) {
			ModusArray[index].shadow = new createjs.Shadow("#000000", 5,5,skB(5));
			ModusArray[index]["button"].graphics.beginFill("green").drawRoundRect(0,0,skB(BreitenArray[index]),skH(buttonH),skB(20));
		}
		else {
			containerWettkampf.shadow = new createjs.Shadow("#000000", 5,5,skB(5));
			buttonWettkampf.graphics.beginFill("green").drawRoundRect(0,0,skB(1040/2),skH(buttonH),skB(20));
		}
	}

	// # Anzahl der Gewichte wird abgespeichert
	function anzahlSpeichern(index) {
		tempZaehler = false;
		if (index==0) anzg = 4;
		if (index==1) anzg = 6;
		if (index==2) anzg = 8;
		if (index==3) {
			anzg = 6;
			tempZaehler = true;
		}
	}

	// # Weiter auf die naechste Stage
	function clickWeiter(event) {

		mehrspieler();
	}

	// # Zurueck zur Modusauswahl
	function clickZurueck(event){

		modus();
	}
}
