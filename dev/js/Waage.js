function waage(stagecont) {

	// --------------Alle Variablen, die immer wieder benutzt werden---------
	//Wettkampfzähler
	var wettkampfzaehler = false;
	//Anzahl Gewichte
	window.anzg;
	window.spielerBeendet = 0;
	//Zaehler erhoeht sich wenn beide leer sind
	var zaehler = 0;
	//Status der Animation. 0=Waage im Ruhezustand, 1=Waage ist links, 2=Waage ist rechts
	var anState = 0;
	//X-Position des ersten und des letzten Gewichts in der Reihe
	var posXmin = skB(180);
	var posXmax = skB(1920-130) - posXmin;
	// Abstand zwischen den Gewichten
	var deltapos = (posXmax - posXmin) / (anzg - 1);

	//Y-Position der Gewichtreihe
	var posY = skH(600);

	//minimale Höhe, maximale Höhe und Breite der Gewichte
	var hmin = skH(0);
	var hmax = skH(220);
	var breiteg = skB(130);

	//Position der Waage (Bild) auf der stage
	var waageposx = skB(710);
	var waageposy = skH(30);

	//Hoehe und Breite der Waageboxen auf der stage
	var breite = skB(130);
	var hoehe = skH(220);

	// Position der Boxen der Waage auf der stage
	var pos1X = waageposx + skB(43);
	var pos1Y = waageposy + skH(168);
	var pos2X = waageposx + skB(330);
	var pos2Y = pos1Y;

	// Waagen Ausschlag
	var tilt = skH(25);
	var tiltmin = pos1Y + tilt;
	var tiltmax = pos1Y - tilt;
	var tilt0 = pos1Y;
	var tilttime = 800;

	//einmal alles löschen, wegen timeout BRAUCHEN WIR DAS NOCH??
	stagecont.removeAllEventListeners();
	stagecont.removeAllChildren();

	// -----------------------Erstelle alle Stageelemente-------------------
	var hintergrund = new createjs.Bitmap("img/HintergrundK.png");
	hintergrund.scaleX = skB(1);
	hintergrund.scaleY = skH(1);
	stagecont.addChild(hintergrund);

	var zaehlerText = new createjs.Text("Anzahl: " + zaehler.toString(), "bold " + skB(32) + "px Calibri", "#000000");
	zaehlerText.textAlign = "center";
	zaehlerText.textBaseline = "middle";
	zaehlerText.x = skB(180) / 2;
	zaehlerText.y = skH(100) / 2;
	zaehlerText.visible = false;

	var cZaehler = new createjs.Container();
	cZaehler.x = skB(30);
	cZaehler.y = skH(30);
	cZaehler.addChild(zaehlerText);
	stagecont.addChild(cZaehler);

	if (tempZaehler) {
		zaehlerText.visible = true;
	}

	// Zurueck Button

	var buttonZurueck = new createjs.Shape();
	buttonZurueck.graphics.beginFill("red").drawRoundRect(0, 0, skB(buttonBreite), skH(buttonHoehe), skB(20));
	buttonZurueck.shadow = new createjs.Shadow("#000000", 5, 5, skB(5));

	var bildZurueck = new createjs.Bitmap("img/pfeillinks.png");
	bildZurueck.scaleX = skB(1);
	bildZurueck.scaleY = skH(1);

	var containerZurueck = new createjs.Container();
	containerZurueck.x = skB(30);
	containerZurueck.y = skH(1080 - buttonHoehe - 30);
	containerZurueck.addChild(buttonZurueck, bildZurueck);
	containerZurueck.addEventListener("click", clickZurueck);

	stagecont.addChild(containerZurueck);

	// Loesen Button
	var buttonLoesen = new createjs.Shape();
	buttonLoesen.graphics.beginFill("#04B404").drawRoundRect(0, 0, skB(buttonBreite), skH(buttonHoehe), skB(20));

	var bildLoesen = new createjs.Bitmap("img/fertig.png");
	bildLoesen.scaleX = skB(1);
	bildLoesen.scaleY = skH(1);

	var containerLoesen = new createjs.Container();
	containerLoesen.x = skB(1920 - buttonBreite - 30);
	containerLoesen.y = skH(1080 - buttonHoehe - 30);
	containerLoesen.addChild(buttonLoesen, bildLoesen);
	containerLoesen.shadow = new createjs.Shadow("#000000", 5, 5, skB(5));
	containerLoesen.addEventListener("click", loesenClick);

	stagecont.addChild(containerLoesen);

	//Im Bubblesort brauchen wir keinen Joker
	if (!bubble) {
		//Joker Button symbolisiert durch Glühbirnen
		var bildJoker = new createjs.Bitmap("img/3tipps.png");
		bildJoker.scaleX = skB(1);
		bildJoker.scaleY = skH(1);

		var containerJoker = new createjs.Container();
		containerJoker.x = skB(1920-335 - 30);
		containerJoker.y = skH(30);
		containerJoker.addChild(bildJoker);
		stagecont.addChild(containerJoker);

		containerJoker.addEventListener("click", jokerClick);
		var jokerCounter = 0;
	}

	//zufällige Position der Elemente
	var randomposition = []
	for (i = 0; i < anzg; i++) {
		randomposition.push(i)
	}

	randomposition = shuffle(randomposition);

	// Daten fuer die Animation der Waage
	var data = {
		images : ["img/animationKomplett.png"],
		frames : {
			width : 500,
			height : 485
		},
		animations : {
			start : 0,
			links : [0, 7, "stand1", 0.2],
			stand1 : 7,
			linksR : [8, 15, "stand2", 0.2],
			stand2 : 15,
			rechts : [16, 23, "stand3", 0.2],
			stand3 : 23,
			rechtsR : [24, 31, "stand4", 0.2],
			stand4 : 31
		}
	};

	// Animation. Faengt im "Ruhezustand" an
	var spriteSheet = new createjs.SpriteSheet(data);
	var animation = new createjs.Sprite(spriteSheet);
	animation.x = waageposx;
	animation.y = waageposy;
	animation.scaleX = skB(1);
	animation.scaleY = skH(1);
	animation.gotoAndPlay("start");
	stagecont.addChild(animation);


	// Erstellen der Gewichte als Bilder + der Ablageflaechen
	var GewichteArray = new Array();
	var ablageboxArray = new Array();
	for (i = 0; i < anzg; i++) {
		var j = randomposition.pop();
		GewichteArray[j] = new Array();
		GewichteArray[j]["x"] = posXmin + (j * deltapos);
		GewichteArray[j]["h"] = hmax;
		GewichteArray[j]["y"] = posY;
		GewichteArray[j]["b"] = breiteg;

		var shape = new createjs.Container();
		shape.x = GewichteArray[j]["x"];
		shape.y = GewichteArray[j]["y"];
		shape.setBounds(0, 0, breiteg, hmax);
		shape["i"] = i;
		GewichteArray[j]["shape"] = shape;

		var gew = new createjs.Bitmap("img/" + (i + 1) + ".png");
		gew.scaleX = skB(1);
		gew.scaleY = skH(1);
		shape["gew"] = gew;
		shape.addChild(gew);

		ablageboxArray[j] = new Array();
		boxshape = new createjs.Shape();
		boxshape.graphics.beginStroke("grey").drawRect(0, 0, breiteg, hmax);
		boxshape.x = posXmin + (j * deltapos);
		boxshape.y = posY;
		boxshape.setBounds(0, 0, breiteg, hmax);
		ablageboxArray[j]["boxshape"] = boxshape;
		stagecont.addChild(ablageboxArray[j]["boxshape"]);
		stagecont.addChild(GewichteArray[j]["shape"]);
	}
	if (!bubble) {
		// Erstellen der Boxen, die die Gewichte verdecken
		for (i = 0; i < anzg; i++) {
			var boxBunt = new createjs.Bitmap("img/BOX" + (i + 1) + ".png");
			boxBunt.alpha = 1;
			boxBunt.scaleX = skB(1);
			boxBunt.scaleY = skH(1);
			GewichteArray[i]["shape"]["boxBunt"] = boxBunt;
			GewichteArray[i]["shape"].addChild(boxBunt);
		}
	}

	//Erstellen der Haken für Hilfe Modus
	if (help) {
		for (i = 0; i < anzg; i++) {
			var haken = new createjs.Bitmap("img/haken.png");
			haken.alpha = 0;
			haken.scaleX = skB(1);
			haken.scaleY = skH(1);
			haken.x = posXmin + (i * deltapos);
			haken.y = posY;
			ablageboxArray[i]["haken"] = haken;
			stagecont.addChild(ablageboxArray[i]["haken"]);
		}
	}

	//Waage Box 1
	// Waage-Box links
	var box1 = new createjs.Shape();

	box1.x = pos1X;
	box1.y = pos1Y;
	box1["filled"] = false;

	var inbox1 = 0;
	box1.setBounds(0, 0, breite, hoehe);
	stagecont.addChild(box1);

	//Waage Box 2
	// Waage-Box rechts
	var box2 = new createjs.Shape();

	box2.x = pos2X;
	box2.y = pos2Y;
	box2["filled"] = false;

	var inbox2 = 0;
	box2.setBounds(0, 0, breite, hoehe);
	stagecont.addChild(box2);

	//Erstellen des Kreuzes bei falschem Lösen
	var kreuz = new createjs.Bitmap("img/kreuz.png");
	kreuz.alpha = 0;
	kreuz.scaleX = skB(1);
	kreuz.scaleY = skH(1);
	kreuz.x = skB(670);
	kreuz.y = skH(200);
	stagecont.addChild(kreuz);


	//Hier rufen wir den Bubblesort auf
	if (bubble) {

		//----------------Variablen vom Bubblesort-------------------------
		//aktuelle beiden Vergleichsboxenindizes
		var erstes = 0;
		var zweites = 1;

		//merkt sich, ob beide Gewichte verglichen wurden und setzt bei ja in der zustandWaageAendern-function gew = 1
		var gew = 0;
		//Variablen speichern die beiden aktuellen ("bunten") Gewichte und die Ablagen worin diese liegen
		var ersteBox;
		var shapeDort1;
		var zweiteBox;
		var shapeDort2;
		//in erg speichern wir uns, ob die beiden Gewichte schon richtig sortiert sind
		var erg;
		//wir speichern noch, ob wir vertauschen oder nicht UND ob danach die beiden Gewichte richtig sortiert sind
		var ergOhneVertauschen = false;
		var ergVertauschen = false;

		//Pfeile in grau und bunt für Bubblesort erstellen
		var grauerPfeil = new Array();
		var bunterPfeil = new Array();
		for (i = 0; i < anzg; i++) {

			grauerPfeil[i] = new createjs.Bitmap("img/Pfeil.png");
			grauerPfeil[i].alpha = 1;
			grauerPfeil[i].scaleX = skB(0.2);
			grauerPfeil[i].scaleY = skH(0.15);
			grauerPfeil[i].x = GewichteArray[i]["x"];
			grauerPfeil[i].y = GewichteArray[i]["y"] + skH(250);
			stagecont.addChild(grauerPfeil[i]);

			bunterPfeil[i] = new createjs.Bitmap("img/Pfeil" + (i + 1) + ".png");
			bunterPfeil[i].alpha = 0;
			bunterPfeil[i].scaleX = skB(0.2);
			bunterPfeil[i].scaleY = skH(0.15);
			bunterPfeil[i].x = GewichteArray[i]["x"];
			bunterPfeil[i].y = GewichteArray[i]["y"] + skH(250);
			stagecont.addChild(bunterPfeil[i]);
		}

		for (i = 0; i < anzg; i++) {

			var boxGrau = new createjs.Bitmap("img/BOXGRAU.png");
			boxGrau.alpha = 1;
			boxGrau.scaleX = skB(1);
			boxGrau.scaleY = skH(1);
			GewichteArray[i]["shape"]["boxGrau"] = boxGrau;
			GewichteArray[i]["shape"].addChild(boxGrau);

			var boxBunt = new createjs.Bitmap("img/BOX"+(i+1)+".png");
			boxBunt.alpha = 0;
			boxBunt.scaleX = skB(1);
			boxBunt.scaleY = skH(1);
			GewichteArray[i]["shape"]["boxBunt"] = boxBunt;
			GewichteArray[i]["shape"].addChild(boxBunt);
		}
		//rufen wir am Anfang einmal von alleine auf
		farbe();
		//wird aufgerufen nachdem 2 Gewichte richtig verglichen wurden und nun die nächsten Beiden dran sind.
		function farbe() {

			//Initialisiere die aktuellen Vergleichsgewichte und setzen sie bunt
			ersteBox = ablageboxArray[erstes]["boxshape"];
			shapeDort1 = inbox(ersteBox, GewichteArray);
			shapeDort1["boxBunt"].alpha = 1;

			zweiteBox = ablageboxArray[zweites]["boxshape"];
			shapeDort2 = inbox(zweiteBox, GewichteArray);
			shapeDort2["boxBunt"].alpha = 1;
			//Die beiden Vergleichsgewichte werden auf bunt gesetzt
			bunterPfeil[erstes].alpha = 1;
			bunterPfeil[zweites].alpha = 1;

			grauerPfeil[erstes].alpha = 0;
			grauerPfeil[zweites].alpha = 0;

			//übergeben pressEvents den Gewichten um diese in der Waage vergleichen zu können
			vergleich();
			ueberpruefe();

		}

		//Überprüft, ob die beiden aktuellen Gewichte richtig sortiert sind.
		function ueberpruefe() {
			//Geben uns die beiden aktuellen Gewichte aus der Ablage aus
			var ersteBoxneu = ablageboxArray[erstes]["boxshape"];
			var shapeDort1neu = inbox(ersteBox, GewichteArray);

			var zweiteBoxneu = ablageboxArray[zweites]["boxshape"];
			var shapeDort2neu = inbox(zweiteBox, GewichteArray);

			if (shapeDort1neu != null && shapeDort2neu != null) {
				//Gewichte liegen in der richtigen Reihenfolge
				if (shapeDort1neu["i"] < shapeDort2neu["i"]) {
					erg = true;

					//erste Instanz speichern um mit späteren Vertauschen zu vergleichen
					if (gew == 0) {
						ergOhneVertauschen = true;
						ergVertauschen = false;
					}

				} else {
					erg = false;

					if (gew == 0) {
						ergVertauschen = true;

					}

				}
			}

			//Wurde in der Waage einmal verglichen(gew=1) und liegen beide in den Boxen und zwar richtig (ergOhneVertauschen== erg) ohne Vertauschen
			if (ergOhneVertauschen == true && erg == true && gew == 1 && shapeDort1neu != null && shapeDort2neu != null) {

				//Farben des linken Pfeils wird auf Grau gesetzt
				grauerPfeil[erstes].alpha = 1;
				bunterPfeil[erstes].alpha = 0;

				shapeDort1["boxBunt"].alpha = 0;
				shapeDort2["boxBunt"].alpha = 0;

				shapeDort1.removeAllEventListeners();
				shapeDort2.removeAllEventListeners();
				//Erhöhen die Indizewerte da nicht vertauscht wurde
				erstes++;
				zweites++;
				//Variablen werden wieder ALLE auf 'false' gesetzt
				erg = false;
				ergOhneVertauschen = false;
				ergVertauschen = false;
				//Neue Gewichte wurden nicht verglichen, also gew auf '0' setzen
				gew = 0;
				//Sind wir am Ende angekommen
				if (erstes == anzg - 1 && zweites == anzg) {
					//Vor beenden setzen wir noch den letzten (rechten) Pfeil auf Grau
					grauerPfeil[zweites - 1].alpha = 1;
					bunterPfeil[zweites - 1].alpha = 0;
					end();

				} else {
					//Rufen farbe() auf um nun die neuen beiden Gewichte zu initialisieren
					farbe();
				}
			}
			//Wurde in der Waage einmal verglichen(gew=1) und liegen beide in den Boxen und zwar richtig (ergVertauschen== erg) mit Vertauschen
			if (ergVertauschen == true && erg == true && gew == 1 && shapeDort1neu != null && shapeDort2neu != null) {

				//Farbe des rechten Pfeils wird auf Grau gesetzt
				grauerPfeil[zweites].alpha = 1;
				bunterPfeil[zweites].alpha = 0;

				shapeDort1["boxBunt"].alpha = 0;
				shapeDort2["boxBunt"].alpha = 0;

				shapeDort1.removeAllEventListeners();
				shapeDort2.removeAllEventListeners();
				//Verringern Indizewerte, da wir vertauscht haben
				erstes--;
				zweites--;
				//Variablen werden wieder ALLE auf 'false' gesetzt
				erg = false;
				ergOhneVertauschen = false;
				ergVertauschen = false;
				//Neue Gewichte wurden nicht verglichen, also gew auf '0' setzen
				gew = 0;
				//Sind wir am Anfang  erhöhen wir immer
				if (erstes < 0 && zweites < 1) {
					//Setzen den ersten Pfeil noch auf Grau
					grauerPfeil[erstes+1].alpha = 1;
					bunterPfeil[erstes+1].alpha = 0;
					//Sonderfall: Da wir nicht weiter nach 'links' können und nicht die gleichen Gewichte immer vergleichen wollen, müssen wir nach 'rechts' gehen
					erstes = 1;
					zweites = 2;

				}
				//Rufen farbe() auf um nun die neuen beiden Gewichte zu initialisieren
				farbe();
			}

		}

		function end() {
			//Lösungsbutton freischalten!
			containerLoesen.addEventListener("click", loesenClick);

		}
		//Hier werden die pressmove-Events auf beiden Gewichten nur aufgerufen, um die Gewichte in der Waage zu vergleichen
		function vergleich() {

			shapeDort1.on("pressmove", function (evt) {
				createjs.Tween.removeTweens(evt.currentTarget);
				window.timeout = window.timeoutvar;
				var shape = evt.currentTarget;
				Object.assign(shape, shape.localToLocal(
					evt.localX - (breite / 2), //Mitte shapes
					evt.localY - (hoehe / 2), //Mitte shapes
					shape.parent
				));
			});

			shapeDort2.on("pressmove", function (evt) {
				createjs.Tween.removeTweens(evt.currentTarget);
				window.timeout = window.timeoutvar;
				var shape = evt.currentTarget;
				Object.assign(shape, shape.localToLocal(
					evt.localX - (breite / 2), //Mitte shapes
					evt.localY - (hoehe / 2), //Mitte shapes
					shape.parent
				));
			});

			shapeDort1.on("pressup", function (evt) {
				window.timeout = window.timeoutvar;
				//Box1
				//Einraste wenn leer
				if (intersect(evt.currentTarget, box1) && box1["filled"] == false) {

					shapeDort1.x = box1.x;
					shapeDort1.y = box1.y;
					box1["filled"] = true;
					//Wenn belegt werfe raus
				} else if (intersect(evt.currentTarget, box1) && box1["filled"] == true) {

					shapeDort1.x = shapeDort1.x - skB(200) * Math.random() - 100;

				}
				//Box2
				//Einraste wenn leer
				if (intersect(evt.currentTarget, box2) && box2["filled"] == false) {

					shapeDort1.x = box2.x;
					shapeDort1.y = box2.y;
					box2["filled"] = true;

					//Wenn belegt werfe raus
				} else if (intersect(evt.currentTarget, box2) && box2["filled"] == true) {

					shapeDort1.x = shapeDort1.x + skB(200) * Math.random() + 100;

				}

				//Ablage Boxen
				//Wenn "welcheablage" null ist, sind wir über keiner box, dann tue nichts
				if (welcheablage(ablageboxArray, evt.currentTarget) != null) {
					//Wenn wir über einer Ablage sind, überprüfe diese Ablage wieviele shapes drin sind.
					//Wenn kein zusätzliches shape, setzte rein
					if (shapesinablage(welcheablage(ablageboxArray, evt.currentTarget), GewichteArray) < 2) {

						shapeDort1.x = welcheablage(ablageboxArray, evt.currentTarget).x;
						shapeDort1.y = welcheablage(ablageboxArray, evt.currentTarget).y;

					}
					//sonst schmeiße raus
					else if (shapesinablage(welcheablage(ablageboxArray, evt.currentTarget), GewichteArray) > 1) {

						shapeDort1.y = posY + skH(250);

					}
				}
				zustandWaageAendern();
			});

			shapeDort2.on("pressup", function (evt) {
				window.timeout = window.timeoutvar;
				//Box1
				//Einraste wenn leer
				if (intersect(evt.currentTarget, box1) && box1["filled"] == false) {

					shapeDort2.x = box1.x;
					shapeDort2.y = box1.y;
					box1["filled"] = true;
					//Wenn belegt werfe raus
				} else if (intersect(evt.currentTarget, box1) && box1["filled"] == true) {

					shapeDort2.x = shapeDort2.x - skB(200) * Math.random() - 100;

				}
				//Box2
				//Einraste wenn leer
				if (intersect(evt.currentTarget, box2) && box2["filled"] == false) {

					shapeDort2.x = box2.x;
					shapeDort2.y = box2.y;
					box2["filled"] = true;
					//Wenn belegt werfe raus
				} else if (intersect(evt.currentTarget, box2) && box2["filled"] == true) {

					shapeDort2.x = shapeDort2.x + skB(200) * Math.random() + 100;

				}

				//Ablage Boxen
				//Wenn "welcheablage" null ist, sind wir über keiner box, dann tue nichts
				if (welcheablage(ablageboxArray, evt.currentTarget) != null) {
					//Wenn wir über einer Ablage sind, überprüfe diese Ablage wieviele shapes drin sind.
					//Wenn kein zusätzliches shape, setzte rein
					if (shapesinablage(welcheablage(ablageboxArray, evt.currentTarget), GewichteArray) < 2) {

						shapeDort2.x = welcheablage(ablageboxArray, evt.currentTarget).x;
						shapeDort2.y = welcheablage(ablageboxArray, evt.currentTarget).y;
					}
					//sonst schmeiße raus
					else if (shapesinablage(welcheablage(ablageboxArray, evt.currentTarget), GewichteArray) > 1) {

						shapeDort2.y = posY + skH(250);

					}
				}
				zustandWaageAendern();
			});
		}
	} else {

		//Alle Elemente können bewegt werden
		//forEach geht jedes element im Array durch, dass durch den Schlüssel assoziiert wird

		GewichteArray.forEach(function (element, index) {
			//Bewegen jedes shapes im Array
			element["shape"].on("pressmove", function (evt) {
				createjs.Tween.removeTweens(element["shape"]);
				window.timeout = window.timeoutvar;
				var shape = evt.currentTarget;
				Object.assign(shape, shape.localToLocal(
					evt.localX - (element["b"] / 2), //Mitte shapes
					evt.localY - (element["h"] / 2), //Mitte shapes
					shape.parent
				));
			});
		});

		//einrasten der Shapes in die jeweiligen Boxen
		GewichteArray.forEach(function (element, index) {
			element["shape"].on("pressup", function (evt) {
				window.timeout = window.timeoutvar;
				//Box1
				//Einraste wenn leer
				if (intersect(evt.currentTarget, box1) && box1["filled"] == false) {

					element["shape"].x = box1.x;
					element["shape"].y = box1.y;
					box1["filled"] = true;
					//Wenn belegt werfe raus
				} else if (intersect(evt.currentTarget, box1) && box1["filled"] == true) {

					element["shape"].x = element["shape"].x - skB(200) * Math.random() - 100;

				}
				//Box2
				//Einraste wenn leer
				if (intersect(evt.currentTarget, box2) && box2["filled"] == false) {

					element["shape"].x = box2.x;
					element["shape"].y = box2.y;
					box2["filled"] = true;
					//Wenn belegt werfe raus
				} else if (intersect(evt.currentTarget, box2) && box2["filled"] == true) {

					element["shape"].x = element["shape"].x + skB(200) * Math.random() + 100;

				}

				//Ablage Boxen
				//Wenn welche "welcheablage" null ist, sind wir über keiner box, dann tue nichts
				if (welcheablage(ablageboxArray, evt.currentTarget) != null) {
					//Wenn wir über einer Ablage sind, überprüfe diese Ablage wieviele shapes drin sind.
					//Wenn kein zusätzliches shape, setzte rein
					if (shapesinablage(welcheablage(ablageboxArray, evt.currentTarget), GewichteArray) < 2) {

						element["shape"].x = welcheablage(ablageboxArray, evt.currentTarget).x;
						element["shape"].y = welcheablage(ablageboxArray, evt.currentTarget).y;

					}
					//sonst schmeiße raus
					else if (shapesinablage(welcheablage(ablageboxArray, evt.currentTarget), GewichteArray) > 1) {

						element["shape"].y = posY + skH(250);

					}
				}
				zustandWaageAendern();
			});

		});
	}

	//ausschlagen der Waage
	function zustandWaageAendern() {
		//Einfärben der Ablagekästen bei richtiger Lage der Gewicht
		if (help) {
			for (i = 0; i < anzg; i++) {
				var aktBox = ablageboxArray[i]["boxshape"];
				var shapeDort = inbox(aktBox, GewichteArray);
				if (shapeDort == null || shapeDort["i"] != i) {
					ablageboxArray[i]["haken"].alpha = 0;
				} else {
					ablageboxArray[i]["haken"].alpha = 1;
				}
			}
		}

		switch (ausschlagen(GewichteArray, box1, box2)) {
			//links beladen, rechts leer
		case 0:
			createjs.Tween.get(box1, {
				loop : false
			}).to({
				y : tiltmin
			}, tilttime, createjs.Ease.linear);
			createjs.Tween.get(box2, {
				loop : false
			}).to({
				y : tiltmax
			}, tilttime, createjs.Ease.linear);
			createjs.Tween.get(inbox(box1, GewichteArray), {
				loop : false
			}).to({
				y : tiltmin
			}, tilttime, createjs.Ease.linear);
			box1.setBounds(0, 0, breite, hoehe);
			box2.setBounds(0, 0, breite, hoehe);

			//nach links ausschlagen
			animationF(1);

			if (wettkampfzaehler) {
				//Anzeige des Zählers erhöht sich
				zaehler++;
				zaehlerText.text = "Anzahl: " + zaehler.toString();
				//Prüft ob Zaehler wieder erhoeht werden soll
				wettkampfzaehler = false;

			}
			box2["filled"] = false;
			break;

			//rechts beladen, links leer
		case 1:
			createjs.Tween.get(box1, {
				loop : false
			}).to({
				y : tiltmax
			}, tilttime, createjs.Ease.linear);
			createjs.Tween.get(box2, {
				loop : false
			}).to({
				y : tiltmin
			}, tilttime, createjs.Ease.linear);
			createjs.Tween.get(inbox(box2, GewichteArray), {
				loop : false
			}).to({
				y : tiltmin
			}, tilttime, createjs.Ease.linear);
			box1.setBounds(0, 0, breite, hoehe);
			box2.setBounds(0, 0, breite, hoehe);

			//nach rechts ausschlagen
			animationF(2);

			if (wettkampfzaehler) {
				//Anzeige des Zählers erhöht sich
				zaehler++;
				zaehlerText.text = "Anzahl: " + zaehler.toString();
				wettkampfzaehler = false;

			}
			box1["filled"] = false;
			break;

			//beide beladen
		case 2:
			//links schwerer als rechts
			if (inbox(box1, GewichteArray)["i"] > inbox(box2, GewichteArray)["i"]) {
				createjs.Tween.get(box1, {
					loop : false
				}).to({
					y : tiltmin
				}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(box2, {
					loop : false
				}).to({
					y : tiltmax
				}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(inbox(box1, GewichteArray), {
					loop : false
				}).to({
					y : tiltmin
				}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(inbox(box2, GewichteArray), {
					loop : false
				}).to({
					y : tiltmax
				}, tilttime, createjs.Ease.linear);
				box1.setBounds(0, 0, breite, hoehe);
				box2.setBounds(0, 0, breite, hoehe);

				//nach links ausschlagen
				animationF(1);
				//Im Bubblesort wurden beide bunten Gewichte verglichen, somit können wir nun überprüfen ob richtig gesetzt wurde
				gew = 1;
				//Waage komplett beladen, also Zähler soll wieder erhöht werden
				wettkampfzaehler = true;

				//Farben der Pfeile werden vertauscht
				//erg gibt uns zurück, ob am Anfang beide schon richtig sortiert waren
				if (!erg && bubble) {
					//Setzen alle Pfeile der beiden Vergleichsgewichte auf "unsichtbar"
					bunterPfeil[erstes].alpha = 0;
					bunterPfeil[zweites].alpha = 0;

					grauerPfeil[erstes].alpha = 0;
					grauerPfeil[zweites].alpha = 0;
					//Speichern 'linken' Pfeil um es mit dem 'rechten' zu vertauschen
					var temp = bunterPfeil[erstes]
						bunterPfeil[erstes] = bunterPfeil[zweites]
						bunterPfeil[zweites] = temp;
					//Bunte Pfeile werden nun wieder "sichtbar" gesetzt
					bunterPfeil[erstes].alpha = 1;
					bunterPfeil[zweites].alpha = 1;
					//Initialisieren die beiden vertauschten Gewichte auf ihre neuen richtigen Werte
					bunterPfeil[erstes].scaleX = skB(0.2);
					bunterPfeil[erstes].scaleY = skH(0.15);
					bunterPfeil[erstes].x = GewichteArray[erstes]["x"];
					bunterPfeil[erstes].y = GewichteArray[erstes]["y"] + skH(250);

					bunterPfeil[zweites].scaleX = skB(0.2);
					bunterPfeil[zweites].scaleY = skH(0.15);
					bunterPfeil[zweites].x = GewichteArray[zweites]["x"];
					bunterPfeil[zweites].y = GewichteArray[zweites]["y"] + skH(250);
				}
			}
			//rechts schwerer als links
			else {
				createjs.Tween.get(box1, {
					loop : false
				}).to({
					y : tiltmax
				}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(box2, {
					loop : false
				}).to({
					y : tiltmin
				}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(inbox(box1, GewichteArray), {
					loop : false
				}).to({
					y : tiltmax
				}, tilttime, createjs.Ease.linear);
				createjs.Tween.get(inbox(box2, GewichteArray), {
					loop : false
				}).to({
					y : tiltmin
				}, tilttime, createjs.Ease.linear);
				box1.setBounds(0, 0, breite, hoehe);
				box2.setBounds(0, 0, breite, hoehe);

				//nach rechts ausschlagen
				animationF(2);
				//Im Bubblesort wurden beide bunten Gewichte verglichen, somit können wir nun überprüfen ob richtig gesetzt wurde
				gew = 1;
				//Waage komplett beladen, also Zähler soll wieder erhöht werden
				wettkampfzaehler = true;

				if (!erg && bubble) {

					bunterPfeil[erstes].alpha = 0;
					bunterPfeil[zweites].alpha = 0;

					grauerPfeil[erstes].alpha = 0;
					grauerPfeil[zweites].alpha = 0;

					var temp = bunterPfeil[erstes]
						bunterPfeil[erstes] = bunterPfeil[zweites]
						bunterPfeil[zweites] = temp;

					bunterPfeil[erstes].alpha = 1;
					bunterPfeil[zweites].alpha = 1;

					bunterPfeil[erstes].scaleX = skB(0.2);
					bunterPfeil[erstes].scaleY = skH(0.15);
					bunterPfeil[erstes].x = GewichteArray[erstes]["x"];
					bunterPfeil[erstes].y = GewichteArray[erstes]["y"] + skH(250);

					bunterPfeil[zweites].scaleX = skB(0.2);
					bunterPfeil[zweites].scaleY = skH(0.15);
					bunterPfeil[zweites].x = GewichteArray[zweites]["x"];
					bunterPfeil[zweites].y = GewichteArray[zweites]["y"] + skH(250);
				}
			}

			break;
			//beide leer
		case 3:
			createjs.Tween.get(box1, {
				loop : false
			}).to({
				y : tilt0
			}, tilttime, createjs.Ease.linear);
			createjs.Tween.get(box2, {
				loop : false
			}).to({
				y : tilt0
			}, tilttime, createjs.Ease.linear);
			box1.setBounds(0, 0, breite, hoehe);
			box2.setBounds(0, 0, breite, hoehe);

			//in die Mitte zurueck
			animationF(0);

			if (wettkampfzaehler) {

				zaehler++;
				zaehlerText.text = "Anzahl: " + zaehler.toString();
				wettkampfzaehler = false;

			}
			box1["filled"] = false;
			box2["filled"] = false;
			//Nur hier überprüfen wir wieder, ob es jetzt richtig sortiert ist.
			if (gew == 1 && bubble) {
				testeErsteBox = ablageboxArray[erstes]["boxshape"];
				testeShapeDort1 = inbox(testeErsteBox, GewichteArray);

				testeZweiteBox = ablageboxArray[zweites]["boxshape"];
				testeShapeDort2 = inbox(testeZweiteBox, GewichteArray);

				//Erst wenn dies erfüllt betrachten wir neue Gewichte!
				if (testeShapeDort1 != null && testeShapeDort2 != null) {
					ueberpruefe();
				}
			}
			break;
		}
	}

	// Endscreen

	var kastenEnde = new createjs.Shape();
	kastenEnde.graphics.beginFill("#04B404").drawRoundRect(0, 0, skB(1920 / 2 + 20), skH(1080 / 2 + 20), skB(20));

	var textEnde = new createjs.Text("SUPER!", "bold " + skB(60) + "px Calibri", "#FFFFFF");
	textEnde.textAlign = "center";
	textEnde.textBaseline = "middle";
	textEnde.x = skB(1920 / 4);
	textEnde.y = skH(1080 / 8);

	var pokal = new createjs.Bitmap("img/pokal.png");
	pokal.scaleX = skB(1);
	pokal.scaleY = skH(1);
	pokal.x = skB(1920 / 4 - 238 / 2);
	pokal.y = skH(1080 / 4 - 70);

	var buttonZurueck1 = new createjs.Shape();
	buttonZurueck1.graphics.beginFill("orange").drawRoundRect(0, 0, skB(180), skH(200), skB(20));

	var bildZurueck1 = new createjs.Bitmap("img/ende.png");
	bildZurueck1.scaleX = skB(1);
	bildZurueck1.scaleY = skH(1);

	var containerZurueck1 = new createjs.Container();
	containerZurueck1.x = skB(30);
	containerZurueck1.y = skH(1080 / 2 - 30 - 200);
	containerZurueck1.addChild(buttonZurueck1, bildZurueck1);
	containerZurueck1.addEventListener("click", clickNeustart);
	containerZurueck1.shadow = new createjs.Shadow("#000000", 5, 5, skB(5));

	var buttonZurueck2 = new createjs.Shape();
	buttonZurueck2.graphics.beginFill("orange").drawRoundRect(0, 0, skB(200), skH(200), skB(20));
	var textZurueck2 = new createjs.Bitmap("img/erneut.png");
	textZurueck2.scaleX = skB(1);
	textZurueck2.scaleY = skH(1);

	var containerZurueck2 = new createjs.Container();
	containerZurueck2.x = skB(1920 / 2 - 30 - 200);
	containerZurueck2.y = skH(1080 / 2 - 30 - 200);
	containerZurueck2.addChild(buttonZurueck2, textZurueck2);
	containerZurueck2.addEventListener("click", clickZurueck);
	containerZurueck2.shadow = new createjs.Shadow("#000000", 5, 5, skB(5));

	var containerEnde = new createjs.Container();
	containerEnde.x = skB(1920 / 4);
	containerEnde.y = skH(30);
	containerEnde.addChild(kastenEnde, textEnde, containerZurueck1, containerZurueck2, pokal);
	stagecont.addChild(containerEnde);
	containerEnde.visible = false;

	function clickZurueck(event) {
		window.timeout = window.timeoutvar;
		//Gelangen nur zurück wenn ALLE Spieler zurückgeklickt haben oder fertig sind
		spielerBeendet++;
		stagecont.removeAllEventListeners();
		stagecont.removeAllChildren();
		if (spielerBeendet == anzahlSpieler) {
			modus();
		}

	}

	function clickNeustart(event) {
		window.timeout = window.timeoutvar;
		//Gelangen nur zurück wenn ALLE Spieler zurückgeklickt haben oder fertig sind
		spielerBeendet++;
		if (spielerBeendet == anzahlSpieler) {
			location.reload();
		}
	}

	function clickErneut(event) {
		window.timeout = window.timeoutvar;
		spielerBeendet++;
		//Erst wenn ALLE fertig sind oder zurück geklickt haben, können wir neu beginnen
		stagecont.removeAllEventListeners();
		stagecont.removeAllChildren();
		if (spielerBeendet == anzahlSpieler) {
			mehrspieler();
		}
	}

	//Loesen Funktion. Wenn der Button gedrueckt wird, soll entschieden werden, ob alle Gewichte richtig sortiert sind.
	function loesenClick(event) {
		window.timeout = window.timeoutvar;
		var erg = true;
		for (i = 0; i < anzg; i++) {
			var aktBox = ablageboxArray[i]["boxshape"];
			var shapeDort = inbox(aktBox, GewichteArray);
			if (shapeDort == null || shapeDort["i"] != i) {
				erg = false;
			}
		}
		if (erg) {
			for (i = 0; i < anzg; i++) {
				var aktbo = GewichteArray[i]["shape"]["boxBunt"];
				createjs.Tween.get(aktbo).to({
					y : -skH(250)
				}, 500, createjs.Ease.linear);
				GewichteArray[i]["shape"].removeAllEventListeners();
			}
			containerZurueck.removeAllEventListeners();
			buttonZurueck.shadow = new createjs.Shadow("#000000", 0, 0, 0);
			containerLoesen.removeAllEventListeners();
			containerLoesen.shadow = new createjs.Shadow("#000000", 0, 0, 0);
			if (!bubble) {
				containerJoker.removeAllEventListeners();
				containerJoker.shadow = new createjs.Shadow("#000000", 0, 0, 0);
			} else {
				for (i = 0; i < anzg; i++) {

					GewichteArray[i]["shape"]["boxGrau"].alpha = 0;

					GewichteArray[i]["shape"]["boxBunt"].alpha = 1;
				}
			}

			containerEnde.visible = true;
			//Erhöhen sich, sobald ein Spieler richtig gelöst hat.
		} else {
			kreuz.alpha = 1;
			setTimeout(function () {
				kreuz.alpha = 0;
			}, 300);
			setTimeout(function () {
				kreuz.alpha = 1;
			}, 600);
			setTimeout(function () {
				kreuz.alpha = 0;
			}, 1700);
		}
	}


		function jokerClick(event) {
			window.timeout = window.timeoutvar;
			if (jokerCounter < 3) {
				for (i = 0; i < anzg; i++) {
					var aktbo = GewichteArray[i]["shape"]["boxBunt"];
					createjs.Tween.get(aktbo).to({
						y : -skH(250)
					}, 500, createjs.Ease.linear);
				}

				setTimeout(runter, 1500);

				var bildJoker = new createjs.Bitmap("img/" + (2 - jokerCounter) + "tipps.png");
				bildJoker.scaleX = skB(1);
				bildJoker.scaleY = skH(1);
				containerJoker.addChild(bildJoker);

				jokerCounter++;
			}

		}


	// alle Boxen werden wieder ueber die Gewichte gesetzt
	function runter() {
		for (i = 0; i < anzg; i++) {
			var aktbo = GewichteArray[i]["shape"]["boxBunt"];
			createjs.Tween.get(aktbo).to({
				y : 0
			}, 500, createjs.Ease.linear);
		}
	}

	// Die Funktion bekommt uebergeben in welche Richtung die Waage ausschlagen soll und ueberprueft,
	// welche Animation dafuer abgespielt werden muss und setzt den Status der Waage neu
	// 0 = Mitte, 1 = Links, 2 = Rechts, anState = aktuelle Position der Waage
	function animationF(richtung) {
		switch (richtung) {
		case 0:
			if (anState == 1)
				animation.gotoAndPlay("linksR");
			else if (anState == 2)
				animation.gotoAndPlay("rechtsR");

			anState = 0;
			break;

		case 1:
			if (anState == 0)
				animation.gotoAndPlay("links");
			else if (anState == 2) {
				animation.gotoAndPlay("rechtsR");
				animation.gotoAndPlay("links");
			}
			anState = 1;
			break;

		case 2:
			if (anState == 0)
				animation.gotoAndPlay("rechts");
			else if (anState == 1) {
				animation.gotoAndPlay("linksR");
				animation.gotoAndPlay("rechts");
			}
			anState = 2;
			break;
		}
	}
}
